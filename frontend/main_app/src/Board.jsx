import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import { Switch, Route, useHistory, withRouter } from "react-router-dom";

import ListOfMessages from "./shared/components/ListOfMessages";
import Compose from 'src/ComposeEmails';
import { OpenedEmail } from "./OpenEmail";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: {
        ...theme.mixins.toolbar,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

const icons = {
    'Received messages': <InboxIcon />,
    'Sent messages': <MailIcon />
}

const paths = {
    'Received messages': '/received',
    'Sent messages': '/sent'
}

const RespDrawer = (props) =>  {
    const { window } = props;
    const classes = useStyles();
    const theme = useTheme();
    let history = useHistory()
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [snack, setSnack] = React.useState(false)

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const handleCloseSnack = () => {setSnack(prev => !prev)}
    const handleOpenSnack = () => {setSnack(true)}

    const drawer = (
        <div>
            <div className={classes.toolbar}>
                <Button variant="outlined" color="secondary" onClick={() => {history.push('/compose')}}>
                    Compose
                </Button>
            </div>
            <Divider/>
            <List>
                {['Received messages', 'Sent messages'].map((text, index) => (
                    <ListItem onClick={() => {history.push(paths[text])}} button key={text}>
                        <ListItemIcon>{icons[text]}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        Messaging system
                    </Typography>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
            <Snackbar open={snack} autoHideDuration={6000} onClose={handleCloseSnack}>
                <Alert onClose={handleCloseSnack} severity="success">
                  The message has been successfully sent!
                </Alert>
            </Snackbar>
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <div className={classes.toolbar} />

                <Switch>
                    <Route exact path="/received">
                        <ListOfMessages
                            sourсeOfMessages='/v1/messages/received'
                        />
                    </Route>
                    <Route path="/sent">
                        <ListOfMessages
                            sourсeOfMessages='/v1/messages/sent'
                        />
                    </Route>
                    <Route path="/compose">
                        <Compose openSnack={handleOpenSnack}/>
                    </Route>
                    <Route path={`/messages/:id`}>
                        <OpenedEmail />
                    </Route>
                </Switch>
            </main>
        </div>
    );
}

RespDrawer.propTypes = {
    window: PropTypes.func,
};

export const ResponsiveDrawer = withRouter(RespDrawer);
