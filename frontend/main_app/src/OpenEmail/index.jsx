import React from 'react';
import Grid from '@material-ui/core/Grid';

import { TitleContainer } from "./Styles";

import api from 'src/shared/utils/api'
import { useParams } from "react-router-dom";

export const OpenedEmail = () => {
    const [message, setMessage] = React.useState({})
    const { id } = useParams()


    React.useEffect(() => {
        api.get(`/v1/messages/${id}`).then(response => {
            if(response.ok){
                response.json().then(message => {
                    setMessage({...message.message})
                })
            }
        })
    }, [])


    return (
        <Grid
            container
            justifyContent="flex-end"
            direction="row"
            alignItems="center"
            spacing={3}
        >
            <Grid xs={12} sm={12} md={9} lg={10} item>
                <TitleContainer>{`${message.sender_id}, ${message.subject}`}</TitleContainer>
                <div>{message.message}</div>
            </Grid>
        </Grid>
    )
}