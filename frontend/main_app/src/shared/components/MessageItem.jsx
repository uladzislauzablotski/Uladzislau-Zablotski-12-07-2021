import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Grid from '@material-ui/core/Grid';

import { useStyles, Sender, Subject, Message, ContainerForSubject } from './Styles'


export const MessageItem = ({ id, subject, message, senderId, handleDelete,
                              handleOpenMessage}) => {
    const styles = useStyles()

    return (
        <ListItem
            classes={{ container: styles.Message }}
            role={undefined}
            dense button onClick={() => {handleOpenMessage(id)}}
        >
            <ListItemText className={styles.MessageInternal}>
                <Grid
                    container
                    justifyContent="space-between"
                    direction="row"
                    alignItems="center"
                >
                        <Grid xs={4} sm={4} md={4} lg={4} item>
                            <Sender>{senderId}</Sender>
                        </Grid>
                        <Grid classes={{root: styles.MessageGrid}} xs={7} sm={7} md={7} lg={7} item>
                            <ContainerForSubject>
                                <Subject>{subject}</Subject>
                            </ContainerForSubject>
                            {' - '}
                            <Message>
                                {message}
                            </Message>
                        </Grid>
                </Grid>
            </ListItemText>
            <ListItemSecondaryAction>
                <IconButton onClick={() => handleDelete(id)} edge="end" aria-label="comments">
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
}