import styled from 'styled-components';
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
    Message: {
        display: 'flex',
    },
    MessageInternal: {
        padding: '10px 0px'
    },
    MessageGrid: {
        display: 'flex'
    },
    UserInput: {
        margin: '10px 0px'
    }
}));


export const Sender = styled.div`
   overflow: hidden;
   text-overflow: ellipsis;
   margin-right: 10px;
`

export const Subject = styled.span`
  text-overflow: ellipsis;
  overflow: hidden;
  flex-grow: 1;
  flex-basis: 0;
  flex-shrink: 1;
  white-space: nowrap;
`

export const Message = styled.span`
  overflow: hidden;
  flex-grow: 1;
  flex-basis: 0;
  flex-shrink: 1;
  text-overflow: ellipsis;
`

export const ContainerForSubject = styled.div`
      min-width: 0;
      flex-shrink: 1;
      display: inline-flex;
      white-space: nowrap;
`

export const ContainerForInformation = styled.div`
    display: flex;
`
