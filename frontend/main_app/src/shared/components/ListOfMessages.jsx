import React from 'react';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from '@material-ui/core/TextField';
import MuiAlert from '@material-ui/lab/Alert';

import { MessageItem } from "src/shared/components/MessageItem";
import api from 'src/shared/utils/api';
import { list_to_dict, list_of_ids } from "src/shared/utils/scripts";

import { useStyles } from "./Styles";
import { useHistory, withRouter } from "react-router-dom";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ListOfMessages = ({ sourсeOfMessages }) => {

    const [messages, setMessages] = React.useState({
        listMessages: [],
        objectMessages: {},
    })
    let history = useHistory()
    const [userId, setUserId] = React.useState(null)

    const [snack, setSnack] = React.useState(false)
    const [uSnack, setUSnack] = React.useState(false)

    const styles = useStyles()

    React.useEffect(() => {
        api.get(`${sourсeOfMessages}/${userId}`).then(response => {
            response.json().then(messages => {
                const result = list_to_dict(messages.messages)
                const ids = list_of_ids(messages.messages)

                setMessages({
                    ...messages,
                    listMessages: ids,
                    objectMessages: result
                })
            })
        })
    }, [userId, sourсeOfMessages])


    const handleDeleteItem = (id) => {
        api.delete(`/v1/messages/${id}`).then(response => {
            if (response.ok){
                const ids = messages.listMessages
                const index = messages.listMessages.indexOf(id)
                delete ids.splice(index, 1)

                setMessages({...messages, listMessages: ids})
                handleSuccessfulDelete()
            }
            else {handleUnsuccessfulDelete()}
        })
    }

    const handleSuccessfulDelete = () => {setSnack(true)}

    const handleUnsuccessfulDelete = () => {setUSnack(true)}

    const handleCloseSnack = () => {setSnack(prev => !prev)}
    const handleCloseUSnack = () => {setUSnack(prev => !prev)}

    const handleOpenMessage = (id) => {
        history.push(`/messages/${id}`)
    }

    return (
            <Grid
                container
                justifyContent="flex-start"
                direction="row"
                alignItems="center"
            >
                <Grid xs={10} sm={8} md={12} lg={12} item>
                    <TextField
                        value={userId? userId : ''}
                        fullWidth={true}
                        id="outlined-basic"
                        label="User ID"
                        className={styles.UserInput}
                        onChange={(event, newValue) => {
                            setUserId(event.target.value)
                        }}
                        variant="outlined"
                    />
                </Grid>
                {messages.listMessages.map(elementId => {
                    if (Object.keys(messages.objectMessages).includes(elementId) > -1){
                    const element = messages.objectMessages[elementId]
                    return (
                        <Grid key={element.id} xs={10} sm={8} md={12} lg={12} item>
                            <MessageItem
                                id={elementId}
                                subject={element.subject}
                                message={element.message}
                                handleOpenMessage={handleOpenMessage}
                                handleDelete={handleDeleteItem}
                                senderId={element.sender_id}
                            />
                        </Grid>
                    )}else { return null }
                })}
                 <Snackbar open={snack} autoHideDuration={3000} onClose={handleCloseSnack}>
                    <Alert onClose={handleCloseSnack} severity="success">
                      The message has been deleted successfully!
                    </Alert>
                  </Snackbar>
                  <Snackbar open={uSnack} autoHideDuration={3000} onClose={handleCloseUSnack}>
                    <Alert onClose={handleCloseUSnack} severity="error">
                      Something went wrong...!
                    </Alert>
                  </Snackbar>
            </Grid>
    )
}

export default withRouter(ListOfMessages);