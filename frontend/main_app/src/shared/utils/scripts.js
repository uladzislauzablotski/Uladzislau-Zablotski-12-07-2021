export const list_to_dict = (list) => {
    // from list of objects to dict of objects {id: object, id2: object}
    const result = list.reduce(function(obj, element){
        obj[element['id']] = element
        return obj
    }, {})


    return result
}

export const list_of_ids = (list) => {
    const result = list.map((element) => element.id)

    return result
}