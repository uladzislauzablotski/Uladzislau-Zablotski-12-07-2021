export const get_current_date = () => {
    const moment = require('moment');
    const today = moment().format('YYYY-MM-DD');

    return today;
};