const api = (method, url, data) =>
    new Promise((resolve, reject) => {
        fetch(url, {
            method: method,
            body: method !== 'GET' ? JSON.stringify(data) : undefined,
            headers: {'Content-Type':'application/json'}
        }).then(
            response => {
                resolve(response);
            },
            error => {
                reject(error)
            }
        )
    });

export default {
    get: (...args) => api('GET', ...args),
    post: (...args) => api('POST', ...args),
    put: (...args) => api('PUT', ...args),
    patch: (...args) => api('PATCH', ...args),
    delete: (...args) => api('DELETE', ...args)
};