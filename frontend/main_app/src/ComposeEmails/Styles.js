import styled from 'styled-components';
import { makeStyles } from "@material-ui/core/styles";

export const TitleContainer = styled.h1`
`

export const useStyles = makeStyles((theme) => ({
    TextField: {
        marginTop: theme.spacing(3),
        width: '100%',
    },
    Button: {
        marginLeft: '10px'
    }
}));


export const ButtonsGroup = styled.div`
    display: flex;
    justify-content: flex-end;
    margin: 10px 0px;
`