import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { TitleContainer, ButtonsGroup, useStyles } from './Styles'
import { get_current_date } from 'src/shared/utils/time'
import api from 'src/shared/utils/api'
import { useHistory, withRouter } from "react-router-dom";

const ComposeEmail = ({openSnack}) => {
    const classes = useStyles()
    let history = useHistory()
    const [message, setMessage] = useState({})
    const [errors, setErrors] = useState({})

    const handleSend = () => {
        const informationToSend = {...message, 'creation_date': get_current_date()}

        api.post('/v1/messages/', informationToSend)
            .then(response => {
                if (response.ok){
                    openSnack()
                    history.push('/received')
                }
                else{response.json().then(errorsResponse => setErrors({...errors, ...errorsResponse}))}
            })
            .catch(response => console.log(response))
    }
    const handleCancel = () => {history.push('/received')}

    const handleChangeField = e => {
        const id = e.target.id
        setErrors({...errors, [id]: null})
        setMessage({...message, [id]: e.target.value})
    }

    return (
        <Grid
            container
            justifyContent="space-around"
            direction="row"
            alignItems="center"
            spacing={3}
        >
            <Grid xs={12} sm={12} md={9} lg={8} item>
                <TitleContainer>Compose a email</TitleContainer>
                <TextField
                    id="subject"
                    error={errors?.subject? true: false}
                    helperText={errors?.subject? errors.subject[0]: ''}
                    variant='outlined'
                    label="Subject"
                    fullWidth={true}
                    onChange={handleChangeField}
                    value={message.subject? message.subject: ''}
                    className={classes.TextField}
                />
                <TextField
                    id="sender_id"
                    variant='outlined'
                    error={errors?.sender_id? true: false}
                    helperText={errors?.sender_id? errors.sender_id[0]: ''}
                    label="Sender ID"
                    value={message.sender_id? message.sender_id: ''}
                    fullWidth={true}
                    onChange={handleChangeField}
                    className={classes.TextField}
                />
                <TextField
                    id="receiver_id"
                    variant='outlined'
                    error={errors?.receiver_id? true: false}
                    helperText={errors?.receiver_id? errors.receiver_id[0]: ''}
                    label="Receiver ID"
                    value={message.receiver_id? message.receiver_id: ''}
                    fullWidth={true}
                    onChange={handleChangeField}
                    className={classes.TextField}
                />
                <TextField
                    id="message"
                    key="message"
                    label="Message"
                    multiline
                    error={errors?.message? true: false}
                    helperText={errors?.message? errors.message[0]: ''}
                    value={message.message? message.message: ''}
                    fullWidth={true}
                    rows={5}
                    variant="outlined"
                    onChange={handleChangeField}
                    className={classes.TextField}
                />
                <ButtonsGroup>
                    <Button
                        className={classes.Button}
                        onClick={handleCancel}
                        variant="outlined"
                        color="primary"
                    >
                        Cancel
                    </Button>
                    <Button
                        className={classes.Button}
                        onClick={handleSend}
                        variant="outlined"
                        color="primary"
                    >
                        Send
                    </Button>
                </ButtonsGroup>
            </Grid>
        </Grid>
    )
}

export default withRouter(ComposeEmail);