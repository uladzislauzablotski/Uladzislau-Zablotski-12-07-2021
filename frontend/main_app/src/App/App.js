import React from 'react';
import { ResponsiveDrawer } from 'src/Board';

import { BrowserRouter as Router, Switch, Route, Link, useHistory, withRouter } from "react-router-dom";

const App = () => {
  return (
      <Router><ResponsiveDrawer/></Router>
  )
}

export default App;
