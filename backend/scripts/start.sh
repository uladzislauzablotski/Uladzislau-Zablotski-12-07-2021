export FLASK_APP=/usr/src/app/main.py
export FLASK_ENV=development
export PYTHONPATH=/usr/src/app

alembic upgrade head

gunicorn main:app