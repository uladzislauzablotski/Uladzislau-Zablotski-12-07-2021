import os
from typing import Optional, Dict, Any

from pydantic import BaseSettings, PostgresDsn, validator


class SettingsPostgres(BaseSettings):
    POSTGRES_HOST: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str

    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str],
                               values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_HOST", '0.0.0.0'),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )


class HerokuPostgres(BaseSettings):
    SQLALCHEMY_DATABASE_URI: Optional[Any] = os.environ.get('DATABASE_URL', '')\
        .replace("://", "ql://", 1)


def _create_settings():
    if os.environ.get('MODE') == "PRODUCTION":
        return HerokuPostgres()

    return SettingsPostgres()


settings = _create_settings()