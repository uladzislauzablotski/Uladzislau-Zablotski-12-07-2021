import abc


class BaseDomainModel(abc.ABC):
    """
    Represents base class for all domain models
    """
    pass
