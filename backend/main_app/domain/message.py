import datetime
from typing import Optional

from .base import BaseDomainModel


class Message(BaseDomainModel):
    def __init__(self, sender_id: str, receiver_id: str, message: str,
                 subject: Optional, creation_date: datetime.date):
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        self.message = message
        self.subject = subject
        self.creation_date = creation_date

