import abc

from sqlalchemy.orm import Session

from domain import BaseDomainModel


class AbstractRepository(abc.ABC):
    def __init__(self, model):
        self.model = model

    def add(self, object_to_add):
        return self._add(object_to_add)

    def get(self, id):
        retrieved_object = self._get(id)

        return retrieved_object

    def list(self):
        return self._list()

    def delete(self, id):
        deleted_object = self._delete(id)

        return deleted_object

    @abc.abstractmethod
    def _add(self, object_to_add):
        raise NotImplementedError

    @abc.abstractmethod
    def _list(self):
        raise NotImplementedError

    @abc.abstractmethod
    def _get(self, id):
        raise NotImplementedError

    @abc.abstractmethod
    def _delete(self, id):
        raise NotImplementedError


class SqlAlchemyRepository(AbstractRepository):
    def __init__(self, model: BaseDomainModel, session: Session):
        super().__init__(model=model)
        self.session = session

    def _add(self, object_to_add: BaseDomainModel):
        self.session.add(object_to_add)

        return object_to_add

    def _list(self):
        return self.session.query(self.model).all()

    def _get(self, id):
        return self.session.query(self.model).filter(self.model.id == id)\
            .first()

    def _delete(self, id):
        retrieved_object = self.session.query(self.model).get(id)
        self.session.delete(retrieved_object)

        return retrieved_object


class MessageRepository(SqlAlchemyRepository):
    def __init__(self, model: BaseDomainModel, session: Session):
        super().__init__(model=model, session=session)

    def get_sent_messages(self, user_id: str):
        return self.session.query(self.model)\
            .filter(self.model.sender_id.contains(user_id)).all()

    def get_received_messages(self, user_id: str):
        return self.session.query(self.model)\
            .filter(self.model.receiver_id.contains(user_id)).all()
