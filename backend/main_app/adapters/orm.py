import logging
from sqlalchemy import (Table, MetaData, Column, Integer, String, Date, Text)
from sqlalchemy.orm import mapper

from domain import Message

logger = logging.getLogger(__name__)

metadata = MetaData()

messages = Table(
    "messages",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("sender_id", String(255), nullable=False),
    Column("receiver_id", String(255), nullable=False),
    Column("message", Text, nullable=False),
    Column("subject", String(255), nullable=False),
    Column("creation_date", Date, nullable=False),
)


def start_mappers():
    logger.info("Starting mappers")

    print('somethong')

    mapper(Message, messages)
