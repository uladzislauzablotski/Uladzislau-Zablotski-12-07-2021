from flask import Blueprint

from . import messages_router

v1_router = Blueprint('v1', __name__, url_prefix='/v1')
v1_router.register_blueprint(messages_router)
