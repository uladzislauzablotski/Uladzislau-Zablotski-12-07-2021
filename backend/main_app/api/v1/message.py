from typing import Any
from flask import Blueprint, request, jsonify
from marshmallow.exceptions import ValidationError as MValidationError

from services import SqlAlchemyUnitOfWork
from services.schemas import MessageRetrieve, MessagePost
from domain import Message

messages_router = Blueprint('messages', __name__, url_prefix='/messages')


@messages_router.route('/', methods=('GET',))
def get_messages():
    with SqlAlchemyUnitOfWork() as uow:
        listed_messages = uow.messages.list()

        objects_to_return = MessageRetrieve(many=True).dump(listed_messages)

    return {'messages': objects_to_return}, 200


@messages_router.route('/<id>', methods=('GET',))
def get_message(id):
    with SqlAlchemyUnitOfWork() as uow:
        listed_messages = uow.messages.get(id=id)

        objects_to_return = MessageRetrieve().dump(listed_messages)

    return {'message': objects_to_return}, 200


@messages_router.route('/sent/<user_id>', methods=('GET',))
def get_sent_messages(user_id):
    with SqlAlchemyUnitOfWork() as uow:
        listed_messages = uow.messages.get_sent_messages(user_id=user_id)

        objects_to_return = MessageRetrieve(many=True).dump(listed_messages)

    return {'messages': objects_to_return}, 200


@messages_router.route('/received/<user_id>', methods=('GET',))
def get_received_messages(user_id):
    with SqlAlchemyUnitOfWork() as uow:
        listed_messages = uow.messages\
            .get_received_messages(user_id=user_id)

        objects_to_return = MessageRetrieve(many=True).dump(listed_messages)

    return {'messages': objects_to_return}, 200


@messages_router.route('/', methods=('POST',))
def create_message():
    received_data = request.json
    try:
        MessagePost().load(data=received_data)
    except MValidationError as exec:
        return jsonify(exec.messages), 400

    message_to_create = Message(**received_data)

    with SqlAlchemyUnitOfWork() as uow:
        created_message = uow.messages.add(object_to_add=message_to_create)
        uow.commit()

        object_to_return = MessageRetrieve().dump(created_message)

    return {'message': object_to_return}, 200


@messages_router.route('/<id>', methods=('DELETE', ))
def delete_message(id: Any):
    with SqlAlchemyUnitOfWork() as uow:
        deleted_message = uow.messages.delete(id=id)
        uow.commit()

        object_to_return = MessageRetrieve().dump(deleted_message)

    return {'message': object_to_return}, 200

