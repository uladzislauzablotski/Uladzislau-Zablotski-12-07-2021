from flask import Flask, redirect

from adapters import orm
from api.v1 import v1_router


def create_app():
    app = Flask(__name__, static_url_path='/static', static_folder='/static/frontend')
    orm.start_mappers()
    app.register_blueprint(v1_router)

    @app.route('/')
    def start():
        return redirect('/received')

    @app.route('/<path>', defaults={'id': None})
    @app.route('/<path>/<id>')
    def home(path, id):
        return app.send_static_file('index.html')

    return app


app = create_app()
