from marshmallow import Schema, fields


class MessageRetrieve(Schema):
    id = fields.Integer()
    sender_id = fields.Str()
    receiver_id = fields.Str()
    message = fields.Str()
    subject = fields.Str()
    creation_date = fields.Date()


class MessagePost(Schema):
    sender_id = fields.Str(required=True)
    receiver_id = fields.Str(required=True)
    message = fields.Str(required=True)
    subject = fields.Str(required=True)
    creation_date = fields.Date(required=True)