from __future__ import annotations
import abc
from typing import NewType, Callable

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from core.config import settings
from domain import Message
from adapters.repository import MessageRepository

Url = NewType("Url of external database", str)


class AbstractUnitOfWork(abc.ABC):
    """
    Class represents external db
    Unit Of Work pattern
    """

    def __enter__(self) -> AbstractUnitOfWork:
        return self

    def __exit__(self, *args):
        self.rollback()

    def commit(self):
        self._commit()

    @abc.abstractmethod
    def _commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self):
        raise NotImplementedError


DEFAULT_SESSION_FACTORY = sessionmaker(
    bind=create_engine(
        settings.SQLALCHEMY_DATABASE_URI,
        isolation_level="REPEATABLE READ",
    )
)


class SqlAlchemyUnitOfWork(AbstractUnitOfWork):
    def __init__(self, session_factory: Callable = DEFAULT_SESSION_FACTORY):
        self.session_factory = session_factory

    def __enter__(self):
        self.session = self.session_factory()
        self.messages = MessageRepository(model=Message, session=self.session)
        return super().__enter__()

    def __exit__(self, *args):
        super().__exit__(*args)
        self.session.close()

    def _commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()


