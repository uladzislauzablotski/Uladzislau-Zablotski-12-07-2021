.PHONY: build
build:
	docker-compose -f docker-compose.local.yml build

.PHONY: run
run:
	docker-compose -f docker-compose.local.yml up