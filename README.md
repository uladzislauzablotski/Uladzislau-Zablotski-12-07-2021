# Uladzislau-Zablotski-12.07.2021
Simple, responsive, web app in client &amp; server that handles messages between users.


The project is written with the help of Flask framework and Postgres database. The client side is written on React + React Hooks(did not use redux as the app os very simple for that). During development i used various patterns such as Unit of Work patter, Repository pattern and focus on folowing DDD design and Solid principles. For migrations i used alembic. Also webpack is used as a bundler.
